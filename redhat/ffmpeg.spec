Name:		ffmpeg_bmat-libs	
Version:	0.5.1
Release:	1%{?dist}
Summary:	Libraries to record, convert and stream audio and video

Group:		System Environment/Libraries
License:	LGPL
URL:		http://ffmpeg.org/
Source0:	%{name}-%{version}.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	zlib-devel

%description
FFmpeg is a very fast video and audio converter. It can also grab from a
live audio/video source.

This package only contains the shared libraries for use in BMAT projects.


%package devel
Summary: Header files and shared library for the ffmpeg codec library
Group: Development/Libraries
Requires: %{name} = %{version}
Requires: zlib-devel, pkgconfig

%description devel
FFmpeg is a very fast video and audio converter. It can also grab from a
live audio/video source.

This package only contains the shared libraries for use in BMAT projects.

Install this package if you want to compile apps with ffmpeg support.


%prep
%setup -q


%build
./configure \
	 --prefix="%{_prefix}" \
	 --libdir="%{_libdir}" \
         --shlibdir="%{_libdir}" \
         --mandir="%{_mandir}" \
         --incdir="%{_includedir}" \
         --enable-shared --disable-static --disable-ffmpeg --disable-ffserver --disable-ffplay --disable-vhook --build-suffix=_bmat
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root, 0755)
%doc Changelog COPYING* CREDITS INSTALL MAINTAINERS README
%{_libdir}/libavcodec_bmat.so.*
%{_libdir}/libavdevice_bmat.so.*
%{_libdir}/libavformat_bmat.so.*
%{_libdir}/libavutil_bmat.so.*

%files devel
%defattr(-,root,root, 0755)
%{_includedir}/libavcodec/
%{_includedir}/libavdevice/
%{_includedir}/libavformat/
%{_includedir}/libavutil/
%{_libdir}/libavcodec_bmat.so
%{_libdir}/libavdevice_bmat.so
%{_libdir}/libavformat_bmat.so
%{_libdir}/libavutil_bmat.so
%{_libdir}/pkgconfig/libavcodec.pc
%{_libdir}/pkgconfig/libavdevice.pc
%{_libdir}/pkgconfig/libavformat.pc
%{_libdir}/pkgconfig/libavutil.pc



%changelog
* Thu Apr 18 2013 BMAT developers <ella-support@bmat.com>
- Initial RPM release.
